## Synopsis

This repository is dedicated to JavaScript code that supports an website supplied with images of Timelapse.Live

## TimeLapse.js

### Description

This script handles the reloading of images on an site. 

The code is supplied in source as well as minified version.

### Code Example

```html
<script src="js/timelapse-live.min.js"></script>

<div class="timelapse-live">
    <img src="http://get.timelapse.live/1605001/01/latest.jpg" data-interval="60">
</div>
```

### Installation

The script can be installed by copying it to your web-server or including the js file from our static mirrors.

To include a live timelapse image on your site, do the following:

- Create an image tag and assign it the class "timelapse-live" (class name is important!)
- Set the parameters by passing `src-data` tags.
- The interval should be equal to the image interval supplied by Timelapse.Live.
