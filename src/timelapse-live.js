/*
 * Timelapse.Live Library for loading live timelapses on a webpage
 */
function initTimelapse (img, interval) {
    var lastUpdate = new Date();

    window.URL = window.URL || window.webkitURL;

    /*
     * Updates the image src element with a object blob
     */
    function updateImage(objURL) {
        var newURL = objURL;
        var oldURL = img.src;

        // Edge case where the URL is the same
        if (oldURL == newURL) {
            console.log("Object URL has not changed, not reloading image");
            return;
        }

        img.src = newURL;

        console.log("New objectURL: ", newURL);

        // Clear the oldURL after we loaded the new one.
        // making sure the url was a blob objectURL
        if (img.src == newURL && oldURL.startsWith('blob:')) {
            window.URL.revokeObjectURL(oldURL);

            console.log("Revoked old objectURL: ", oldURL);
        }
    }

    /*
     * Requests a new image on our GET endpoint and compares the modified times
     */
    function fetchImage (url) {
        var xhr = new XMLHttpRequest();

        xhr.onreadystatechange = function(){
            if(this.readyState == this.DONE) {
                // The 200 is after following the API's 302
                if (this.status == 200) {
                    lastModified = new Date(this.getResponseHeader("Last-Modified"));

                    if (lastUpdate.getTime() < lastModified.getTime()) {
                        // Update the lastModified timestamp
                        lastUpdate = lastModified;

                        // Load the new image into the browser
                        console.log("New image received: ", this.response, typeof this.response);
                        updateImage(window.URL.createObjectURL(this.response));
                    }
                }

                // If no image is found, leave the last one in place
                if (this.status == 404) {
                    console.log("New image not available yet");
                }
            }
        };

        xhr.open('GET', url);

        // We need a blob as response.
        xhr.responseType = 'blob';
        xhr.send();
    }

    setInterval(fetchImage, interval * 1000, img.src);
}


/*
 * Initializes all timelapse elements on the page
 */
document.addEventListener('DOMContentLoaded', function() {
    var timelapses = document.querySelectorAll('.timelapse-live img');

    for (var i = 0, element; element = timelapses[i]; i++) {
        var interval = parseInt(element.dataset.interval) || 60;
        initTimelapse(element, interval);
    }
});
